#include <fcntl.h>
#include <net/if.h>
#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <time.h>
#include <unistd.h>
// wifiname
#include <linux/wireless.h>
#include <sys/ioctl.h>
#include <sys/socket.h>

#include <X11/Xlib.h>

#define AC_STATUS "ACAD"
#define BATT_STATUS "BAT1"
#define IW_INTERFACE "wlp2s0"
#define ETHERNET "enp3s0"
#define TIME_FORMAT "| %a %d %b | %H:%M |"
#define NOTIFICATION_PIPE_NAME "bar"
#define REFRESH_TIME 1 // in seconds
#define NOTIFY_TIME 4  // in seconds
#define status_max_size 128
#define notify_max_size 128
#define FILESIZE 16
#define date_long sizeof(TIME_FORMAT) + 2
#define read_int(F) atoi(readfile(F, 0))
#define getethernet (getnet(ETHERNET)) ? "ETHERNET  " : ""
#define getusb (access("/proc/scsi/usb-storage", F_OK) == 0) ? "  " : ""
#define BAT_IS_CHARGING read_int("/sys/class/power_supply/" AC_STATUS "/online")
#define BATT_NOW read_int("/sys/class/power_supply/" BATT_STATUS "/capacity")
#define fifo_name "/tmp/" NOTIFICATION_PIPE_NAME ".fifo"
#define mutex_do(S)                                                            \
  pthread_mutex_lock(&stop_threads);                                           \
  S;                                                                           \
  pthread_mutex_unlock(&stop_threads)

static Display *dpy;
// mutex var
int shared = 0;
int fd;
pthread_mutex_t stop_threads = PTHREAD_MUTEX_INITIALIZER;

static void change_status(const char *str) {
  XStoreName(dpy, DefaultRootWindow(dpy), str);
  XSync(dpy, False);
}

void *get_notity(void *param) {
  char buf[notify_max_size];
  int ft;
  while (1) {
    // read the FIFO
    ft = open(fifo_name, O_RDONLY);
    read(ft, buf, sizeof(buf));
    close(ft);
    // set delay time 4
    mutex_do(shared = NOTIFY_TIME);
    // print message
    change_status(buf);
    // cleaning buf
    memset(&buf[0], 0, sizeof(buf));
  }
  // return 0;
}

static char *readfile(const char *file, int n) {
  static char buf[FILESIZE];
  memset(&buf, 0, sizeof(buf));
  if (n == 0 || n > FILESIZE)
    n = FILESIZE;

  fd = open(file, O_RDONLY);
  if (fd == -1 || read(fd, buf, n) == 0) {
    strcpy(buf, "0");
    printf("Error archivo %s\n", file);
  }
  close(fd);
  return buf;
}

static const char *current_date(void) {
  static char date[24];
  time_t now = time(0);
  strftime(date, 24, TIME_FORMAT, localtime(&now));
  return date;
}

static const char *bat_draw(void) {
  int level = BATT_NOW;
  static const char battery_icons[10][5] = {"LOW", "", "", "", "",
                                            "",   "", "", "", ""};
  static char status[11];
  if (BAT_IS_CHARGING) {
    sprintf(status, " %d%%", level);
  } else {
    sprintf(status, "%s %d%%", battery_icons[(level / 10) - 1], level);
  }
  return status;
}

static const int getnet(char *INTERFACE) {
  char CARD_PATH[strlen(INTERFACE) + 26];
  sprintf(CARD_PATH, "/sys/class/net/%s/operstate", INTERFACE);
  return strcmp(readfile(CARD_PATH, 4), "down");
}

// static const char *wifiname(void) {
//   if (getnet(IW_INTERFACE)) {
//     struct iwreq wreq;
//     static char id[IW_ESSID_MAX_SIZE + 6];
//     memset(&wreq, 0, sizeof(struct iwreq));
//     strcpy(wreq.ifr_name, IW_INTERFACE);
//     wreq.u.essid.length = IW_ESSID_MAX_SIZE + 1;
//     if (!((fd = socket(AF_INET, SOCK_DGRAM, 0)) < 0)) {
//       wreq.u.essid.pointer = id;
//       if (!(ioctl(fd, SIOCGIWESSID, &wreq) < 0)) {
//         close(fd);
//         return id;
//       }
//     }
//     close(fd);
//   }
//   return "";
// }
static const char *get_wifi() {

  struct ifreq net_interface;
  struct iwreq wreq;
  static char id[IW_ESSID_MAX_SIZE + 6];

  memset(&net_interface, 0, sizeof(struct ifreq));
  strcpy(net_interface.ifr_ifrn.ifrn_name, IW_INTERFACE);
  memset(&wreq, 0, sizeof(struct iwreq));
  strcpy(wreq.ifr_name, IW_INTERFACE);
  wreq.u.essid.length = IW_ESSID_MAX_SIZE + 1;
  wreq.u.essid.pointer = id;

  if ((fd = socket(AF_INET, SOCK_DGRAM, 0)) < 0 ||
      ioctl(fd, SIOCGIFFLAGS, &net_interface) < 0)
    goto error;

  if (net_interface.ifr_ifru.ifru_flags & IFF_UP) {
    if (net_interface.ifr_ifru.ifru_flags & IFF_RUNNING) {
      if (!(ioctl(fd, SIOCGIWESSID, &wreq) < 0)) {
        close(fd);
        return id;
      }
    }
    close(fd);
    return " ¿?";
  }

error:
  close(fd);
  return "";
}

/*static const char *getethernet(void) {
  return (getnet(ETHERNET)) ? "ETHERNET  " : "";
}*/

/*static const char *getusb(void) {
  return (access("/proc/scsi/usb-storage", F_OK) == 0) ? "  " : "";
}*/

int main(void) {
  char *status = malloc(status_max_size);

  if (!(dpy = XOpenDisplay(NULL))) {
    printf("Error open display.\n");
    return 1;
  }
  if (access(fifo_name, F_OK) != 0) {
    printf("creating fifo .\n");
    mkfifo(fifo_name, 0600);
  }
  pthread_t thread;
  pthread_create(&thread, NULL, &get_notity, NULL);

  while (1) {
    pthread_mutex_lock(&stop_threads);
    if (shared == 0) {
      sprintf(status, "%s%s%s %s %s ", getusb, get_wifi(), getethernet,
              current_date(), bat_draw());
      change_status(status);
    } else {
      --shared;
    }
    pthread_mutex_unlock(&stop_threads);
    // memset(status, 0, sizeof(*status));
    sleep(REFRESH_TIME);
  }
  XCloseDisplay(dpy);

  return 0;
}
