#include <fcntl.h>
#include <stdio.h>
#include <string.h>
#include <sys/stat.h>
#include <unistd.h>

#define MAXTR 128
#define FIFO "/tmp/bar.fifo"

int main(int argn, char *argv[]) {

  int fd;
  char buf[MAXTR];
  if (argn == 1) {
    printf("Uso:  notify \"menssagmenssagee\"\n");
    return 1;
  }
  if (argn > 2) {
    printf(
        "Demaciados argumentos\n------->Uso:  notify \"menssagmenssagee\"\n");
    return 1;
  }
  if (strlen(argv[1]) > MAXTR) {
    printf("Menssage max size: %d\nActual size: %ld\n", MAXTR, strlen(argv[1]));
    return 1;
  }

  strncpy(buf, argv[1], MAXTR);
  // strncpy(buf, argv[1], MAXTR);
//  printf("%c", buf[sizeof(buf) - 1]);
  fd = open(FIFO, O_WRONLY);
  if (fd != 3) {
    printf("can't access to fifo\n");
    close(fd);
    return 1;
  }
  write(fd, buf, strlen(buf));
  close(fd);
  return 0;
}
