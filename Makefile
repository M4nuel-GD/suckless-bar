BAR = suckless-bar
NOTIFY = notify
#PREFIX = /usr/local
PREFIX = ~/.local
CC = cc

all: BAR_C NOTIFY_C

BAR_C = ${BAR}.c
NOTIFY_C = ${NOTIFY}.c

BAR_C:
	@echo "Building ${BAR_C}"
	@${CC} -std=c99 -pedantic -Wall -Ofast -s -pthread -lX11 $(BAR_C) -o ${BAR}  && echo -e '\e[32mDONE!!\e[0m\n'

NOTIFY_C:
	@echo "Building ${NOTIFY_C}"
	@${CC} -std=c99 $(NOTIFY_C) -o ${NOTIFY} && echo -e '\e[32mDONE!!\e[0m\n'


clean:
	@echo cleaning
	@rm -f ${NOTIFY} ${BAR}

install: all
	@echo installing in ${DESTDIR}${PREFIX}/bin
	@mkdir -p ${DESTDIR}${PREFIX}/bin
	@cp -f ${NOTIFY} ${BAR} ${DESTDIR}${PREFIX}/bin
	@chmod 755 ${DESTDIR}${PREFIX}/bin/${NOTIFY}
	@chmod 755 ${DESTDIR}${PREFIX}/bin/${BAR}

uninstall:
	@echo Uninstalling from ${DESTDIR}${PREFIX}/bin
	@rm -f ${DESTDIR}${PREFIX}/bin/${NAME}

.PHONY: all options clean install uninstall
